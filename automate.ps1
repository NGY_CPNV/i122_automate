﻿<#

.SYNOPSIS
This is a Power shell script simulating a drinks automate

.DESCRIPTION
As user, you can add coins, select product or ask the automate to retrieve your coins.

.EXAMPLE
./automate.ps1 2 # add 2 swiss francs
./automate.ps1 -stop # stop the process and retrieve coins
./automate.ps1 -select 4 # select the forth drink in the automate
./automate.ps1 2 -select 4 -stop #cumulate all possible actions

After each action, the program will display the current credit.

.NOTES
    Please refere to the sequence diagram to get a visual aspect of the script logic
    Name: automate.ps1 
    Author: nicolas.glassey at cpnv.ch
    Requires: Tested with PowerShell v5 only.
              Oldest version will interpret #region as simple comment (and not as code region).
	Version History:
	1.0 - 20/06/2017 - Initial Release.
    1.1 - 20/06/2017 - Code review (global variables removed from function by parameters)
    1.2 - 21/06/2017 - Case drink not available and not found display the current credit

.LINK
https://youtu.be/rcU2gH_3kt4 - Specifications and tests
https://youtu.be/vzgheFTL53w - Flow diagram
https://youtu.be/55EpJKb8p-w - Power Shell demo

#>

#region parameters
Param
(
    [float]$coinToInsert = -1, 
    [switch]$stop,
    [int]$select
)
#endregion parameters

#region -global- variables and constants
#constants
Set-Variable -Name ACCEPTED_COINS -Option Constant -Value (0.5,1,2,5);
Set-Variable -Name DB_FILE -Option Constant -Value "$pSScriptRoot/current_credit.csv"
Set-Variable -Name AUTOMATE_CONTENT -Option Constant -Value "$pSScriptRoot/automate_content.csv";
Set-Variable -Name MAX_AUTOMATE_SIZE -Option Constant -Value 20;

$global:drinks = $null
[float]$global:currentCredit = 0.0;
#endregion -global- variables and constants

#region init
#Function setCredit
#This function is designed to set the current credit
#Param
#[string] $dbFile. Get the path to find the DB File containing the credit.  
#Return
#void
function setCredit($dbFile)
{
    if (Test-Path ($dbFile))
    {
        $lastCredit = Import-Csv $DB_File -Delimiter ";"
        $lastCredit | ForEach-Object{
            $global:currentCredit = [float]$_.Current_Credit;
        }
    }
}

#Function loadAutomate
#This function is designed to load the automate content
#Param
#[int] $maxSize. Will be use to check if we try to overload the automate
#[sting] $contentToLoad. Get the path to find the automate content to load.  
#Return
#void
function loadAutomate($maxSize, $contentToLoad)
{
    $global:drinks = IMPORT-CSV -Delimiter ";" -Path "$contentToLoad";
    if ($global:drinks.Count -gt $maxSize -OR $global:drinks -eq $null)
    {
        $global:drinks = $null;
        Write-Host "Automate will be empty. Error during products loading (Reminder : Capacity max = $maxSize)";
    }
}

#we set the current credit
setCredit($DB_File);

#we load the automate
loadAutomate($MAX_AUTOMATE_SIZE) ($AUTOMATE_CONTENT);
#endregion init

#region manage credit
#Function updateCredit
#This function is update the credit available
#Param
#[float] $amount. The amount to add/remove to the current credit 
#Return
#void
function updateCredit([float]$amount)
{
    #user gets a drink
    if ($amount -lt 0)
    {
        $global:currentCredit += $amount;
    }
    #user add coin
    elseif (validateCoins($amount) -eq $true)
    {
        $global:currentCredit += $amount;
    }
    updateFileAndDisplay($global:currentCredit);
}

#Function moneyBack
#This function is designed to get the money back to the automate'suser
#Param
#none  
#Return
#void
function moneyBack()
{
    #if coins still be in the automate, we get its back
    if ($global:currentCredit -gt 0)
    {
        Write-Host "Money back ... Cling, Cling, Cling";
    }
    #set the current credit to 0
    $global:currentCredit = 0.0;
    updateFileAndDisplay($global:currentCredit);
}

#Function updateFileAndDisplay
#This function is designed to update credit (db file and automate display)
#Param
#[float] $credit. Current credit.
#Return
#[void]
function updateFileAndDisplay($credit)
{
    #update database file and display with new credit
    writeIntoFile($credit);
    Write-Host "Your credit : " $global:currentCredit;
}

#Function validateCoins
#This function is designed to check if the inserted coin is valide.
#Param
#[float] $coinToValidated. Coin inserted by the user.
#Return
#[bool] either true if the coin is accepted or false if rejected
function validateCoins([float]$coinToValidated)
{
    foreach ($coins in $ACCEPTED_COINS){
        if($coins -eq $coinToValidated)
        {
            #the coin inserted by the user is accepted
            return $true;
        }
    }
    #user coin rejected
    #TO DO - generate info message in relation with the const $ACCEPTED_COINS
    Write-Host "Coin rejected or absent. Only Swiss Coins are allowed  (0.5 / 1 / 2 / 5)";
    return $false;
}

#Function writeIntoFile
#This function is designed to update the file content (DB FILE)
#Param
#[float] $credit. Current credit available for the user
#Return
#[void]
function writeIntoFile($credit)
{
    $msg = "Current_Credit`r`n" + $credit;
    $msg | Out-File -FilePath $DB_File;
}
#endregion manage credit

#region manage drinks
#Function getDrinks
#This function is delivered drink.
#Param
#[array] $listOfDrinks. Automate content (list of drinks).
#[int] $drinkToDeliver. User's choice (drink).
#[float] $userCredit. User's available credit.
#Return
#[void]
function getDrinks($listOfDrinks, $drinkToDeliver, $userCredit)
{
    foreach ($drink in $listOfDrinks)
    {
        if ($drink.ID -eq $drinkToDeliver)
        {
            if ($drink.QTY -ge 1)
            {
                $drinkCost = $drink.PRICE;
                if ($drinkCost -le $userCredit)
                {
                    #update user credit
                    Write-Host "Drink $drinkToDeliver delivered";
                    updateCredit(-$drinkCost);
                    return;
                }
                else
                {
                    #credit not enough for this drink choice
                    Write-Host "Drink not delivered. Credit not enough.";
                    [float]$missingCredit = $drinkCost - $userCredit;
                    Write-Host "Missing     : $missingCredit";
                    moneyBack;
                    return;
                }   
            }
            else
            {
                Write-Host "This drink is not available";
                updateFileAndDisplay($userCredit);
                return;
            }
        }
    }
    Write-Host "Drink not found";
    updateFileAndDisplay($userCredit);
}
#endregion manage drinks

#region user's choice
#check if user wants to insert coin
if ($coinToInsert -ge 0)
{
    updateCredit($coinToInsert);
}

#check if user wants to select a drink
if ($drinks.Count -ge $select -and $select -gt 0)
{
    #$userChoice = $select;
    getDrinks($global:drinks)($select)($global:currentCredit);
}

#check if user wants to stop the process
if ($stop)
{
    #get money back
    moneyBack;
    exit;
}
#endregion user's choice